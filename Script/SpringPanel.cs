﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class SpringPanel : MonoBehaviour
{
    public Transform target;
    public float speed;
    private void OnMouseDrag()
    {
        Quaternion q = target.transform.localRotation;
        q.y += speed*Time.deltaTime;        
        target.transform.localRotation = q;
    }
}
