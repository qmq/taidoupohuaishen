﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InfoType
{
    Name,
    HeadPortrait,
    Level,
    Power,
    Exp,
    Diamond,
    Coin,
    Energy,
    Toughen,
    All
}

public class PlayerInfo : MonoBehaviour {
    public static PlayerInfo Instance;

    #region 属性
    private string _name;
    private string _headPortrait;
    private int _level = 1;
    private int _power = 1;
    private int _exp = 0;
    private int _diamond = 0;
    private int _coin = 0;
    private int _energy;
    private int _toughen;
    #region get set methd
    public string Name
    {
        get
        {
            return _name;
        }

        set
        {
            _name = value;
        }
    }

    public string HeadPortrait
    {
        get
        {
            return _headPortrait;
        }

        set
        {
            _headPortrait = value;
        }
    }

    public int Level
    {
        get
        {
            return _level;
        }

        set
        {
            _level = value;
        }
    }

    public int Power
    {
        get
        {
            return _power;
        }

        set
        {
            _power = value;
        }
    }

    public int Exp
    {
        get
        {
            return _exp;
        }

        set
        {
            _exp = value;
        }
    }

    public int Diamond
    {
        get
        {
            return _diamond;
        }

        set
        {
            _diamond = value;
        }
    }

    public int Coin
    {
        get
        {
            return _coin;
        }

        set
        {
            _coin = value;
        }
    }

    public int Energy
    {
        get
        {
            return _energy;
        }

        set
        {
            _energy = value;
        }
    }

    public int Toughen
    {
        get
        {
            return _toughen;
        }

        set
        {
            _toughen = value;
        }
    }




    #endregion

    #endregion


    private float energyTimer = 0;
    private float toughenTimer = 0;

    public delegate void OnPlayerInfoChangedEvent(InfoType type);
    public event OnPlayerInfoChangedEvent OnPlayerInfoChanged;

    #region unity event
    private void Awake()
    {
        Instance = this;
    }
    // Use this for initialization
    void Start () {
        Init();
	}
	
	// Update is called once per frame
	void Update () {
	    if(Energy<100)
        {
            energyTimer += Time.deltaTime;
            if (energyTimer > 60)
            {
                Energy += 1;
                energyTimer -=60;
                OnPlayerInfoChanged(InfoType.Energy);
            }
        }else
        {
            energyTimer = 0;
        }

        if (Toughen < 50)
        {
            toughenTimer += Time.deltaTime;
            if (toughenTimer > 60)
            {
                Toughen += 1;
                toughenTimer -= 60;
                OnPlayerInfoChanged(InfoType.Toughen);
            }
            else
            {
                toughenTimer = 0;
            }
        }
	}
    #endregion

    void Init()
    {
        Coin = 9870;
        Diamond = 1234;
        Energy = 30;
        Exp = 123;
        HeadPortrait = "头像底板男性";
        Level = 12;
        Name = "柔小美";
        Power = 1745;
        Toughen = 34;
        OnPlayerInfoChanged(InfoType.All);
    }


}
