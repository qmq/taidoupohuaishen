﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBar : MonoBehaviour {


    private Image HeadSprite;
    private Text LevelLabel;
    private Text NameLabel;
    private Scrollbar EnergyProgressScrollbar;
    private Text EnergyLabel;
    private Scrollbar ToughenProgressScrollbar;
    private Text ToughenLabel;
    private Button EnergyPlusButton;
    private Button ToughenPlusButton;

    private void Awake()
    {
        HeadSprite = transform.Find("HeadSprite").GetComponent<Image>();
        LevelLabel = transform.Find("LevelLabel").GetComponent<Text>();
        NameLabel = transform.Find("NameLabel").GetComponent<Text>();
        EnergyProgressScrollbar = transform.Find("EnergyProgressScrollbar").GetComponent<Scrollbar>();
        EnergyLabel = EnergyProgressScrollbar.GetComponentInChildren<Text>();
        ToughenProgressScrollbar = transform.Find("ToughenProgressScrollbar").GetComponent<Scrollbar>();
        ToughenLabel = ToughenProgressScrollbar.GetComponentInChildren<Text>();

        EnergyPlusButton = transform.Find("EnergyPlusButton").GetComponent<Button>();
        ToughenPlusButton = transform.Find("ToughenPlusButton").GetComponent<Button>();
        
    }

    // Use this for initialization
    void Start () {
        PlayerInfo.Instance.OnPlayerInfoChanged += OnPlayerInfoChanged;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnDestroy()
    {
        PlayerInfo.Instance.OnPlayerInfoChanged -= OnPlayerInfoChanged;
    }

    void OnPlayerInfoChanged(InfoType type)
    {
        if (type==InfoType.All||type == InfoType.Name || type == InfoType.HeadPortrait || type == InfoType.Energy || type == InfoType.Toughen||type==InfoType.Level)
        {
            UpdateShow();
        }
    }

    void UpdateShow()
    {
        PlayerInfo info = PlayerInfo.Instance;
        HeadSprite.sprite = Resources.Load<Sprite>("002-mainmenu"+info.HeadPortrait);
        LevelLabel.text = info.Level.ToString();
        NameLabel.text = info.Name;
        EnergyProgressScrollbar.size = info.Energy / 100f;
        EnergyLabel.text = info.Energy + "/100";
        ToughenProgressScrollbar.size = info.Toughen / 50f;        
        ToughenLabel.text = info.Toughen + "/50";
    }
}
