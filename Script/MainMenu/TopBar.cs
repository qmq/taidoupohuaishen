﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopBar : MonoBehaviour {
    private Text CoinLabel;
    private Text DiamondLabel;
    private Button CoinPlusButton;
    private Button DiamonPlusButton;

    private void Awake()
    {
        CoinLabel = transform.Find("CoinBg/CoinLabel").GetComponent<Text>();
        DiamondLabel = transform.Find("DiamondBg/DiamondLabel").GetComponent<Text>();
        CoinPlusButton = transform.Find("CoinBg/CoinPlusButton").GetComponent<Button>();
        DiamonPlusButton = transform.Find("DiamondBg/DiamonPlusButton").GetComponent<Button>();
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
