﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatus : MonoBehaviour {

    private Image HeadSprite;
    private Text LevelLabel;
    private Text NameLabel;
    private Text PowerLabel;
    private Scrollbar ExpScrollbar;
    private Text expLabel;
    private Button ButtonChangeName;
    private Button ButtonClose;
    private Text DiamondLabel;
    private Text CoinLabel;
    private Text energyLabel;
    private Text energyTime;
    private Text energyAllTime;
    private Text toughenLabel;
    private Text toughenTime;
    private Text toughenAllTime;


    private void Awake()
    {
        HeadSprite = transform.Find("HeadSprite").GetComponent<Image>();
        LevelLabel = transform.Find("LevelLabel").GetComponent<Text>();
        NameLabel = transform.Find("NameLabel").GetComponent<Text>();
        PowerLabel = transform.Find("PowerLabel").GetComponent<Text>();
        ExpScrollbar = transform.Find("ExpScrollbar").GetComponent<Scrollbar>();
        expLabel = transform.Find("ExpScrollbar/expLabel").GetComponent<Text>();
        ButtonChangeName = transform.Find("ButtonChangeName").GetComponent<Button>();
        ButtonClose = transform.Find("ButtonClose").GetComponent<Button>();
        DiamondLabel = transform.Find("Box/DiamondBox/DiamondLabel").GetComponent<Text>();
        CoinLabel = transform.Find("Box/CoinBox/CoinLabel").GetComponent<Text>();
        energyLabel = transform.Find("EnergyPanel/energyLabel").GetComponent<Text>();
        energyTime = transform.Find("EnergyPanel/energyTime").GetComponent<Text>();
        energyAllTime = transform.Find("EnergyPanel/energyAllTime").GetComponent<Text>();
        toughenLabel = transform.Find("ToughenPanel/toughenLabel").GetComponent<Text>();
        toughenTime = transform.Find("ToughenPanel/toughenTime").GetComponent<Text>();
        toughenAllTime = transform.Find("ToughenPanel/toughenAllTime").GetComponent<Text>();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
