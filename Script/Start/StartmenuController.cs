﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartmenuController : MonoBehaviour {

    public static StartmenuController Instance;


    public Animator start;
    public Animator login;
    public Animator register;
    public Animator server;
    public Animator characterselect;
    public Animator charactershowselect;

    public Text usernameInputLogin;
    public Text passwordInputLogin;
    public Text usernameStart;

    public Text usernameInputRegister;
    public Text passwordInputRegister;
    public Text repasswordInputRegister;
    public Text serverSelected;

    public Transform serverListGrid;
    public GameObject serverItemRed;
    public GameObject serverItemGreen;

    public static string username;
    public static string password;
    public static ServerProperty sp;    



    private bool haveInitServerlist = false;

    public GameObject serverSelectGo;


    public GameObject[] characterArray;

    private GameObject characterSelected;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        InitServerlist();//初始化服务器列表
    }


    public void OnUsernameCilck()
    {

        //输入帐号登陆
        start.SetTrigger("isOn");
        StartCoroutine(HidePlane(start.gameObject));
        login.gameObject.SetActive(true);

    }

    public void OnServerCilck()
    {
        start.gameObject.SetActive(false);
        server.gameObject.SetActive(true);
        server.Play("back");

        //InitServerlist();//初始化服务器列表
    }

    public void OnEnterGameClick()
    {
        //1连接服务器，验证用户名和服务器

        //2进入角色选择界面
        start.gameObject.SetActive(false);
        characterselect.gameObject.SetActive(true);
    }

    /// <summary>
    /// 隐藏面板
    /// </summary>
    IEnumerator HidePlane(GameObject go)
    {
        yield return new WaitForSeconds(1f);
        go.SetActive(false);
    }

    public void OnLoginClick()
    {
        
        //得到用户名和密码，存储
        username = usernameInputLogin.text;
        password = passwordInputLogin.text;
        //返回开始界面
        login.gameObject.SetActive(false);
        start.gameObject.SetActive(true);
        start.Play("back");
        if (username.Length > 0)
        {
            usernameStart.text = username;
        }        
    }

    public void OnRegisterShowClick()
    {
        login.gameObject.SetActive(false);
        register.gameObject.SetActive(true);
        register.Play("back");
    }

    public void OnLoginCloseClick()
    {
        login.gameObject.SetActive(false);
        start.gameObject.SetActive(true);
        start.Play("back");
    }

    public void OnCancelClick()
    {
        register.gameObject.SetActive(false);
        login.gameObject.SetActive(true);
    }

    public void OnRegisterCloseClick()
    {
        OnCancelClick();
    }

    public void OnRegisterAndLoginClick()
    {
        //1连接服务器进行验证

        //2连接失败

        //3连接成功 保存用户名与密码
        username = usernameInputRegister.text;
        password = usernameInputRegister.text;

        register.gameObject.SetActive(false);
        start.gameObject.SetActive(true);
        start.Play("back");
        if (username.Length > 0) { 
            usernameStart.text = username;
        }
    }

    public void InitServerlist()
    {
        if (haveInitServerlist) return;
        //1连接服务器，取得游戏服务器列表信息
        //2根据上面的信息，添加服务器列表

        for(int i = 0; i < 20; i++)
        {
            string ip = "127.0.0.1:9080";
            string name = (i + 1) + "区 马达加斯加";
            int count = Random.Range(0, 100);
            GameObject go = null;
            if (count > 50)
            {
                //火爆
                go = Instantiate<GameObject>(serverItemRed, serverListGrid, false);
            }else
            {
                //流畅
                go = Instantiate<GameObject>(serverItemGreen, serverListGrid, false);
            }
            ServerProperty sp = go.GetComponent<ServerProperty>();
            sp.ip = ip;
            sp.title = name;
            sp.count = count;
        }
        haveInitServerlist = true;
    }

    public void OnServerselect(GameObject go)
    {
        sp = go.GetComponent<ServerProperty>();
        serverSelectGo.GetComponent<Image>().sprite = go.GetComponent<Image>().sprite;
        serverSelectGo.transform.Find("Text").GetComponent<Text>().text = sp.title;
    }

    public void OnServerSelectedClose()
    {
        server.gameObject.SetActive(false);
        start.gameObject.SetActive(true);
        start.Play("back");
        serverSelected.text = sp.title;
    }

 
    public void OnCharacterClick(GameObject go)
    {
        if (characterSelected == go)
        {
            return;
        }

        iTween.ScaleTo(go, new Vector3(1.5f, 1.5f, 1.5f), 0.5f);
        if (characterSelected != null)
        {
            iTween.ScaleTo(characterSelected, new Vector3(1.0f, 1.0f, 1.0f), 0.5f);
        }
        characterSelected = go;
    }

    public void OnButtonChangeCharacterClick()
    {
        characterselect.gameObject.SetActive(false);
        charactershowselect.gameObject.SetActive(true);
    }

    public void OnBackClick()
    {

        Hashtable ht = new Hashtable();
        
        

        charactershowselect.gameObject.SetActive(false);
        characterselect.gameObject.SetActive(true);
    }
}


