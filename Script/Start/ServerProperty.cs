﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ServerProperty : MonoBehaviour {
    public string ip = "127.0.0.1:7878";
    private string _title;
    public string title
    {
        set
        {
            transform.Find("Text").GetComponent<Text>().text = value;
            _title = value;
        }
        get
        {
            return _title;
        }
    }
    public int count=100;

    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(delegate
        {
            transform.root.SendMessage("OnServerselect", gameObject);
        });
    }

}
